use std::fs::File;
use std::net::SocketAddrV4;

use futures::Future;
use tor_file_parser::DescIter;

use error::{StemError, StemResult};
use reply::{Command, Reply};
use socket::TorCtrlSocket;
use traits::CircuitPath;
use types::{CircuitEvent, EventType, ServerSpec, ServerSpecIter};

#[derive(Debug)]
pub struct TorCtrl {
    pub socket: TorCtrlSocket,
    pub monitored_events: Vec<EventType>,
    pub using_extended_events: bool
}

impl TorCtrl {
    pub fn new(port: u16) -> StemResult<Self> {
        let socket = TorCtrlSocket::connect(port)?;
        let monitored_events = vec![];
        let using_extended_events = false;
        let tc = TorCtrl { socket, monitored_events, using_extended_events };
        Ok(tc)
    }

    pub fn cmd(&mut self, mut cmd: String) -> StemResult<Vec<String>> {
        if !cmd.ends_with('\n') {
            cmd.push('\n');
        }
        let bytes = cmd.as_bytes();
        let _ = self.socket.send(bytes).wait()?;
        self.socket.recv_reply_lines()
    }

    pub fn auth(&mut self, auth_token: Option<&str>) -> StemResult<Reply> {
        let auth_token = auth_token.unwrap_or("");
        let cmd = format!("AUTHENTICATE \"{}\"", auth_token);
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::Authenticate);
        Ok(reply)
    }

    pub fn get_conf(&mut self, keyword: &str) -> StemResult<Reply> {
        let cmd = format!("GETCONF {}", keyword);
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::GetConf);
        Ok(reply)
    }

    pub fn get_data_directory(&mut self) -> StemResult<String> {
        let reply = self.get_conf("DataDirectory")?;

        if !reply.is_250() {
            return Err(StemError::Non250StatusCode);
        }

        let dir = reply.data_lines()[0]
            .trim_left_matches("DataDirectory=")
            .to_string();

        Ok(dir)
    }

    pub fn set_conf(&mut self, keyword: &str, value: &str) -> StemResult<Reply> {
        let cmd = format!("SETCONF {}={}", keyword, value);
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::SetConf);
        Ok(reply)
    }

    pub fn disable_predicted_circuits(&mut self) -> StemResult<()> {
        self.set_conf("__DisablePredictedCircuits", "1").map(|_| ())
    }

    pub fn enable_predicted_circuits(&mut self) -> StemResult<()> {
        self.set_conf("__DisablePredictedCircuits", "0").map(|_| ())
    }

    pub fn get_info(&mut self, keyword: &str) -> StemResult<Reply> {
        let cmd = format!("GETINFO {}", keyword);
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::GetInfo);
        Ok(reply)
    }

    pub fn get_socks_addr(&mut self) -> StemResult<SocketAddrV4> {
        let resp = self.get_info("net/listeners/socks")?;
        let addr = resp.data_lines()[0].trim_matches('"').parse().unwrap();
        Ok(addr)
    }

    pub fn get_socks_port(&mut self) -> StemResult<u16> {
        let port = self.get_socks_addr()?.port();
        Ok(port)
    }

    pub fn get_protocol_info(&mut self) -> StemResult<Reply> {
        let cmd = "PROTOCOLINFO".to_string();
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::ProtocolInfo);
        Ok(reply)
    }

    pub fn set_events(&mut self, event_types: &[EventType]) -> StemResult<Reply> {
        let joined_events = event_types.iter()
            .map(|e| e.as_str())
            .collect::<Vec<_>>()
            .join(" ");

        let cmd = format!("SETEVENTS {}", joined_events);

        self.cmd(cmd).map(|resp_lines| {
            let reply = Reply::new(resp_lines, Command::SetEvents);

            if reply.is_250() {
                self.using_extended_events = false;
                self.monitored_events = event_types.to_vec();
            }

            reply
        })
    }

    pub fn set_events_extended(&mut self, event_types: &[EventType]) -> StemResult<Reply> {
        let joined_events = event_types.iter()
            .map(|e| e.as_str())
            .collect::<Vec<_>>()
            .join(" ");

        let cmd = format!("SETEVENTS EXTENDED {}", joined_events);

        self.cmd(cmd).map(|resp_lines| {
            let reply = Reply::new(resp_lines, Command::SetEvents);

            if reply.is_250() {
                self.using_extended_events = true;
                self.monitored_events = event_types.to_vec();
            }

            reply
        })
    }

    /// Add an `EventType` to the list of async-events we are receiving
    /// from the Tor Control server.
    pub fn add_event(&mut self, event_type: EventType) -> StemResult<()> {
        let mut event_types = self.monitored_events.clone();

        if !event_types.contains(&event_type) {
            event_types.push(event_type);
        }

        if self.using_extended_events {
            self.set_events_extended(&event_types).map(|_| ())
        } else {
            self.set_events(&event_types).map(|_| ())
        }
    }

    /// Stop receiving async-events for a single `EventType`.
    pub fn remove_event(&mut self, event_type: EventType) -> StemResult<()> {
        let mut event_types = self.monitored_events.clone();
        event_types.remove_item(&event_type);

        if self.using_extended_events {
            self.set_events_extended(&event_types).map(|_| ())
        } else {
            self.set_events(&event_types).map(|_| ())
        }
    }

    /// Get a list of currently built circuits.
    pub fn get_circuits(&mut self) -> StemResult<Vec<CircuitEvent>> {
        let reply = self.get_info("circuit-status")?;

        if !reply.is_250() {
            return Err(StemError::Non250StatusCode);
        }

        let circuits = reply.data_lines().iter()
            .map(|line| CircuitEvent::from_str(line))
            .filter(|circuit| circuit.is_built())
            .collect();

        Ok(circuits)
    }

    pub fn get_circuit_by_id(&mut self, circ_id: u16) -> StemResult<CircuitEvent> {
        for circuit in self.get_circuits()? {
            if circuit.id == circ_id {
                return Ok(circuit);
            }
        }

        Err(StemError::CircuitDoesNotExistError)
    }

    pub fn get_internal_circuits(&mut self) -> StemResult<Vec<CircuitEvent>> {
        let mut circuits = self.get_circuits()?;
        circuits.retain(|circuit| circuit.is_internal());
        Ok(circuits)
    }

    pub fn get_non_internal_circuits(&mut self) -> StemResult<Vec<CircuitEvent>> {
        let mut circuits = self.get_circuits()?;
        circuits.retain(|circ| !circ.is_internal());
        Ok(circuits)
    }

    pub fn close_circuit(&mut self, circ_id: u16) -> StemResult<Reply> {
        let cmd = format!("CLOSECIRCUIT {}", circ_id);
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::CloseCircuit);
        Ok(reply)
    }

    pub fn close_non_internal_circuits(&mut self) -> StemResult<()> {
        for circ in self.get_non_internal_circuits()? {
            let _ = self.close_circuit(circ.id)?;
        }
        Ok(())
    }

    pub fn get_guards_in_use(&mut self) -> StemResult<Vec<ServerSpec>> {
        let mut guards = vec![];

        for circuit in self.get_circuits()? {
            if let Some(guard) = circuit.get_guard() {
                if !guards.contains(guard) {
                    guards.push(guard.clone());
                }
            }
        }

        Ok(guards)
    }

    pub fn get_exits_in_use(&mut self) -> StemResult<Vec<ServerSpec>> {
        let mut exits = vec![];

        for circuit in self.get_circuits()? {
            if let Some(exit) = circuit.get_exit() {
                if !exits.contains(exit) {
                    exits.push(exit.clone());
                }
            }
        }

        Ok(exits)
    }

    pub fn extend_circuit<T>(&mut self, mut circ_id: u16, extension_path: T) -> StemResult<Reply>
        where T: CircuitPath
    {
        if extension_path.is_empty() {
            return Err(StemError::EmptyCircuitPathError);
        }

        let joined_fps = extension_path.join_fps()?;
        let cmd = format!("EXTENDCIRCUIT {} {}", circ_id, joined_fps);
        let resp_lines = self.cmd(cmd)?;
        let reply = Reply::new(resp_lines, Command::ExtendCircuit);

        if reply.is_552() {
            return Err(StemError::CircuitDoesNotExistError);
        } else if !reply.is_250() {
            return Err(StemError::Non250StatusCode);
        }

        if circ_id == 0 {
            circ_id = reply.data_lines()[0].parse().unwrap();
        }

        let mut attached_temporary_listener = false;

        if !self.monitored_events.contains(&EventType::Circ) {
            let _ = self.add_event(EventType::Circ)?;
            attached_temporary_listener = true;
        }

        let mut result: StemResult<Reply> = Ok(reply);
        let mut received_final_event = false;

        while !received_final_event {
            let circ_event = self.socket.recv_circuit_event();
            if circ_event.id != circ_id { continue; }
            let extend_failed = circ_event.has_failed();

            if extend_failed {
                result = Err(StemError::CircuitStatusFailedError);
            }

            received_final_event = extend_failed || circ_event.is_built();
        }

        if attached_temporary_listener {
            let _ = self.remove_event(EventType::Circ);
        }

        result
    }

    pub fn create_circuit<T: CircuitPath>(&mut self, path: T) -> StemResult<u16> {
        let reply = self.extend_circuit(0, path)?;
        let circ_id = reply.data_lines()[0].parse().unwrap();
        Ok(circ_id)
    }

    pub fn all_exits(&mut self) -> StemResult<Vec<ServerSpec>> {
        let data_dir = self.get_data_directory()?;
        let path = format!("{}/cached-microdesc-consensus", data_dir);

        let file = File::open(path)
            .map_err(|_| StemError::FileDoesNotExist)?;

        let exits = DescIter::from_reader(&file)
            .filter_map(|desc|
                if desc.is_exit() {
                    Some(ServerSpec::from(desc))
                } else {
                    None
                }
            )
            .collect();

        Ok(exits)
    }

    pub fn all_guards(&mut self) -> StemResult<Vec<ServerSpec>> {
        let data_dir = self.get_data_directory()?;
        let path = format!("{}/cached-microdesc-consensus", data_dir);

        let file = File::open(path)
            .map_err(|_| StemError::FileDoesNotExist)?;

        let exits = DescIter::from_reader(&file)
            .filter_map(|desc|
                if desc.is_guard() {
                    Some(ServerSpec::from(desc))
                } else {
                    None
                }
            )
            .collect();

        Ok(exits)
    }

    pub fn all_routers(&mut self) -> StemResult<ServerSpecIter<File>> {
        let data_dir = self.get_data_directory()?;
        let path = format!("{}/cached-microdesc-consensus", data_dir);

        let file = File::open(path)
            .map_err(|_| StemError::FileDoesNotExist)?;

        let desc_iter = DescIter::from_reader(file);
        let iter = ServerSpecIter::new(desc_iter);
        Ok(iter)
    }
}
