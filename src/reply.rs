use std::collections::HashMap;
use std::str::FromStr;

use types::EventType;

const VALID_STATUS_CODES: [u16; 17] = [
    250,  251,  451,  500,  510,
    511,  512,  513,  514,  515,
    550,  551,  552,  553,  554,
    555,  650
];

// This trait is useful for working with a stringified line that has been
// received from the Tor Control Server.
pub trait ServerResponseLine {
    fn check_for_event_type(&self) -> Option<EventType>;
    fn is_status_line(&self) -> bool;
    fn check_for_status_code(&self) -> Option<u16>;
    fn strip_status_code(&self) -> &str;
    fn split_into_args_and_kwargs(&self) -> (Vec<String>, HashMap<String, String>);

    fn is_event_line(&self) -> bool {
        self.check_for_event_type().is_some()
    }

    fn is_reply_line(&self) -> bool {
        !self.is_event_line()
    }

    fn contains_status_code(&self) -> bool {
        self.check_for_status_code().is_some()
    }
}

// We implement the `ServerResponseLine` interface for all types whose
// reference can be coersed to an &str in order to utilize many of
// str's methods (such as `.trim()` and `.split()` and `.contains()`)
// when a function is generic over `T: ServerResponseLine`.
impl<T> ServerResponseLine for T
    where T: AsRef<str>
{
    fn check_for_event_type(&self) -> Option<EventType> {
        let s = self.as_ref();
        EventType::from_str(s).ok()
    }

    fn is_status_line(&self) -> bool {
        if self.is_event_line() { return false; }
        let first_word = self.as_ref().split(' ').nth(0).unwrap();

        if let Ok(num) = first_word.parse() {
            if VALID_STATUS_CODES.contains(&num) {
                return true;
            }
        }

        false
    }

    fn check_for_status_code(&self) -> Option<u16> {
        let num = self.as_ref()
            .split(|c| c == ' ' || c == '-' || c == '+')
            .nth(0).unwrap()
            .parse().ok()?;

        if VALID_STATUS_CODES.contains(&num) {
            Some(num)
        } else {
            None
        }
    }

    fn strip_status_code(&self) -> &str {
        let s = self.as_ref();

        if s.starts_with("250-") {
            s.trim_left_matches("250-")
        } else if s.starts_with("250+") {
            s.trim_left_matches("250+")
        } else if self.contains_status_code() {
            s.splitn(2, ' ').nth(1).unwrap()
        } else {
            s
        }
    }

    fn split_into_args_and_kwargs(&self) -> (Vec<String>, HashMap<String, String>) {
        let mut args = vec![];
        let mut kwargs = HashMap::new();

        for word in self.as_ref().split(' ') {
            if word.trim_matches('=').contains('=') {
                let mut split = word.splitn(2, '=');
                let key = split.next().unwrap().to_string();
                let value = split.next().unwrap().to_string();
                kwargs.insert(key, value);
            } else {
                args.push(word.to_string());
            }
        }

        (args, kwargs)
    }
}

#[derive(Debug)]
pub enum Command {
    Authenticate,
    GetConf,
    SetConf,
    SetEvents,
    GetInfo,
    CloseCircuit,
    ExtendCircuit,
    ProtocolInfo
}

#[derive(Debug)]
pub struct Reply {
    pub status_code: u16,
    command: Command,
    lines: Vec<String>,
    n_lines: usize
}

impl Reply {
    pub fn new(lines: Vec<String>, command: Command) -> Self {
        let n_lines = lines.len();
        let last_index = n_lines - 1;
        let status_code = lines[last_index].check_for_status_code().unwrap();
        Reply { status_code, command, lines, n_lines }
    }

    pub fn is_250(&self) -> bool {
        self.status_code == 250
    }

    pub fn is_552(&self) -> bool {
        self.status_code == 552
    }

    pub fn lines(&self) -> &Vec<String> {
        &self.lines
    }

    pub fn data_lines(&self) -> Vec<String> {
        match self.command {
            Command::Authenticate |
            Command::SetConf |
            Command::CloseCircuit |
            Command::SetEvents => vec![],
            Command::GetConf => self.parse_get_conf_data(),
            Command::GetInfo => self.parse_get_info_data(),
            Command::ExtendCircuit => self.parse_extend_circuit_data(),
            Command::ProtocolInfo => self.parse_protocol_info_data(),

        }
    }

    fn parse_get_conf_data(&self) -> Vec<String> {
        let mut data_lines = vec![];

        for line in &self.lines {
            let data_line = line.strip_status_code().to_string();
            if !data_line.contains('=') { continue; }
            data_lines.push(data_line);
        }

        data_lines
    }

    fn parse_get_info_data(&self) -> Vec<String> {
        let mut data_lines = vec![];
        let is_single_line = self.lines[0].starts_with("250-");
        let first_line_contains_data = !self.lines[0].ends_with('=');

        if is_single_line && first_line_contains_data {
            let data_line = self.lines[0].split('=').nth(1).unwrap().to_string();
            data_lines.push(data_line);
        } else {
            let stop = self.n_lines - 2;
            for line in &self.lines[1..stop] {
                let data_line = line.to_string();
                data_lines.push(data_line);
            }
        }

        data_lines
    }

    fn parse_extend_circuit_data(&self) -> Vec<String> {
        let data = self.lines[0].trim_left_matches("250 EXTENDED ").to_string();
        vec![data]
    }

    fn parse_protocol_info_data(&self) -> Vec<String> {
        let mut data_lines = vec![];
        let stop = self.n_lines - 1;

        for line in &self.lines[..stop] {
            let data_line = line.strip_status_code().to_string();
            data_lines.push(data_line);
        }

        data_lines
    }

    // fn as_key_value_pairs(&self) -> Vec<(String, String)> {}
    // fn as_hashmap(&self) -> HashMap<String, String> {}
}
