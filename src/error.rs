use std::io;

pub type StemResult<T> = Result<T, StemError>;

#[derive(Debug)]
pub enum StemError {
    SocketConnectError,
    SocketWriteError,
    SocketRecvLineError,
    Non250StatusCode,
    EmptyServerSpecError,
    ServerSpecMissingFp,
    ServerSpecFromStrError,
    EmptyCircuitPathError,
    CircuitDoesNotExistError,
    InvalidHiddenServiceState,
    CircuitStatusFailedError,
    FileDoesNotExist,
    TorProcessError(io::Error),
    TorStartupError(Vec<String>)
}
