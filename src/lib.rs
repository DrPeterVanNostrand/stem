// Enable th experimental vector method `.remove_item()`.
#![feature(vec_remove_item)]

extern crate futures;
extern crate futures_cpupool;
extern crate tor_file_parser;

mod control;
mod error;
mod process;
mod reply;
mod socket;
mod traits;
mod types;
mod utils;

pub use control::TorCtrl;
pub use error::{StemError, StemResult};
pub use process::TorProcess;
pub use reply::{Command, Reply, ServerResponseLine};
pub use socket::TorCtrlSocket;
pub use traits::CircuitPath;
pub use types::{
    CircuitBuildFlag,
    CircuitEvent,
    CircuitPurpose,
    CircuitReason,
    CircuitStatus,
    EventType,
    ServerSpec
};
pub use utils::{
    duration_to_seconds,
    split_into_args_kwargs
};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() { assert!(true); }
}
