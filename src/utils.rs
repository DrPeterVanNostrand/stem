use std::collections::HashMap;
use std::time::Duration;

pub fn duration_to_seconds(dt: Duration) -> f64 {
    let n_secs = dt.as_secs() as f64;
    let n_nanosecs = dt.subsec_nanos() as f64 / 10u32.pow(9) as f64;
    n_secs + n_nanosecs
}

pub fn split_into_args_kwargs(s: &str) -> (Vec<String>, HashMap<String, String>) {
    let mut args = vec![];
    let mut kwargs = HashMap::new();

    for el in s.split_whitespace() {
        if el.contains('=') {
            let mut split = el.splitn(2, '=');
            let key = split.next().unwrap().to_string();
            let value = split.next().unwrap().to_string();
            kwargs.insert(key, value);
        } else {
            args.push(el.to_string());
        }
    }

    (args, kwargs)
}
