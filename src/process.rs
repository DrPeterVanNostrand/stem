use std::io::{BufRead, BufReader};
use std::process::{Child, Command, Stdio};

use error::{StemError, StemResult};

#[derive(Debug)]
pub struct TorProcess(Child);

impl TorProcess {
    pub fn launch() -> StemResult<Self> {
        let mut child = Command::new("tor")
            .stdout(Stdio::piped())
            .spawn()
            .map_err(|e| StemError::TorProcessError(e))?;

        let mut launch_succeeded = false;
        let mut stdout_lines = vec![];

        if let Some(ref mut stdout) = child.stdout {
            for line_res in BufReader::new(stdout).lines() {
                let line = line_res.unwrap();
                if line.contains("Bootstrapped 100%") {
                    launch_succeeded = true;
                    break;
                }
                stdout_lines.push(line)
            }
        }

        if launch_succeeded {
            Ok(TorProcess(child))
        } else {
            Err(StemError::TorStartupError(stdout_lines))
        }
    }
}

impl Drop for TorProcess {
    fn drop(&mut self) {
        let _ = self.0.kill();
    }
}
