use std::io::{BufRead, BufReader, BufWriter, Write};
use std::net::TcpStream;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use futures::Future;
use futures_cpupool::{CpuFuture, CpuPool};

use error::{StemError, StemResult};
use reply::ServerResponseLine;
use types::{CircuitEvent, EventType};

#[derive(Debug)]
pub struct TorCtrlSocket {
    stream: TcpStream,
    thread_pool: CpuPool,
    reader_ptr: Arc<Mutex<BufReader<TcpStream>>>,
    writer_ptr: Arc<Mutex<BufWriter<TcpStream>>>
}

impl TorCtrlSocket {
    pub fn connect(port: u16) -> StemResult<Self> {
        let addr = format!("127.0.0.1:{}", port);

        let stream = TcpStream::connect(&addr)
            .map_err(|_| StemError::SocketConnectError)?;

        let ten_seconds = Duration::new(10, 0);
        let _ = stream.set_read_timeout(Some(ten_seconds)).unwrap();

        let thread_pool = CpuPool::new_num_cpus();

        let reader = BufReader::new(stream.try_clone().unwrap());
        let writer = BufWriter::new(stream.try_clone().unwrap());

        let reader_ptr = Arc::new(Mutex::new(reader));
        let writer_ptr = Arc::new(Mutex::new(writer));

        let socket = TorCtrlSocket { stream, thread_pool, reader_ptr, writer_ptr };
        Ok(socket)
    }

    /// Remove the TCP Socket's read timeout.
    pub fn remove_read_timeout(&mut self) {
        let _ = self.stream.set_read_timeout(None).unwrap();
    }

    /// Set the TCP Socket's read timeout in milliseconds.
    pub fn set_read_timeout_millis(&mut self, n_millis: u64) {
        let duration = Duration::from_millis(n_millis);
        let _ = self.stream.set_read_timeout(Some(duration)).unwrap();
    }

    /// Set the TCP Socket's read timeout in seconds.
    pub fn set_read_timeout_secs(&mut self, n_secs: u64) {
        let duration = Duration::from_secs(n_secs);
        let _ = self.stream.set_read_timeout(Some(duration)).unwrap();
    }

    /// Send some bytes to the Tor Control server. Until the bytes have
    /// been successfully sent, the polled future will return `Async::NotReady`.
    /// This function is designed to be non-blocking to allow for future
    /// async integration.
    pub fn send(&mut self, bytes: &[u8]) -> CpuFuture<(), StemError> {
        let writer_ptr = self.writer_ptr.clone();
        let bytes = bytes.to_vec();

        let send_future = self.thread_pool.spawn_fn(move || {
            let mut writer = writer_ptr.lock().unwrap();

            writer.write_all(&bytes)
                .map_err(|_| StemError::SocketWriteError)?;

            let _ = writer.flush();

            let res: Result<(), StemError> = Ok(());
            res
        });

        send_future
    }

    /// Receive a line from the TCP stream. If there is no data to be read
    /// from the socket, the polled future will return `Async::NotReady`.
    /// If there is data to be read from the socket, a line will be read
    /// from the socket and converted to a String, if any of the bytes read
    /// are invalid UTF-8, then an Err will be returned. This function is
    /// designed to be non-blocking to allow for future async integration.
    pub fn recv_line(&mut self) -> CpuFuture<String, StemError> {
        let reader_ptr = self.reader_ptr.clone();

        let recv_line_future = self.thread_pool.spawn_fn(move || {
            let mut line = String::new();
            let mut reader = reader_ptr.lock().unwrap();

            let _ = reader.read_line(&mut line)
                .map_err(|_| StemError::SocketRecvLineError)?;

            line = line.trim_right_matches("\r\n").to_string();
            let res: Result<String, StemError> = Ok(line);
            res
        });

        recv_line_future
    }

    /// Receive a Reply immediately following with the issuance a command
    /// sent to the Tor Control Server. If any Event lines are received
    /// in the time between sending the command and receiving its synchronous
    /// Reply, for now, we ignore them. Asynchonous Event lines will never be
    /// interspersed with a synchonous Reply (control-spec, section 4.2).
    pub fn recv_reply_lines(&mut self) -> StemResult<Vec<String>> {
        let mut reached_status_line = false;
        let mut lines = vec![];

        while !reached_status_line {
            let line = self.recv_line().wait()?;
            reached_status_line = line.is_status_line();

            if line.is_reply_line() {
                lines.push(line);
            }
        }

        Ok(lines)
    }

    /// Read lines from the socket until a line starting with:
    /// "650 <event name>" is received. Return the line as a String.
    /// Note that this method is blocking.
    pub fn recv_event_line(&mut self) -> String {
        self.remove_read_timeout();
        loop {
            if let Ok(line) = self.recv_line().wait() {
                if line.is_event_line() {
                    self.set_read_timeout_secs(10);
                    return line;
                }
            }
        }
    }

    /// Read lines from the socket until a line starting with: "650 CIRC"
    /// is received. Return a `CircuitEvent` instance constructed from
    /// that line. Note that this method is blocking.
    pub fn recv_circuit_event(&mut self) -> CircuitEvent {
        loop {
            let line = self.recv_event_line();
            if let Some(EventType::Circ) = line.check_for_event_type() {
                return CircuitEvent::from_str(&line);
            }
        }
    }
}
