use error::{StemError, StemResult};
use types::ServerSpec;

pub trait CircuitPath {
    fn n_servers(&self) -> usize;
    fn is_empty(&self) -> bool { self.n_servers() == 0 }
    fn join_fps(&self) -> StemResult<String>;
}

impl CircuitPath for ServerSpec {
    fn n_servers(&self) -> usize { 1 }

    fn join_fps(&self) -> StemResult<String> {
        if let Some(ref fp) = self.fp {
            Ok(fp.to_string())
        } else {
            Err(StemError::ServerSpecMissingFp)
        }
    }
}

impl<'a> CircuitPath for &'a ServerSpec {
    fn n_servers(&self) -> usize { 1 }

    fn join_fps(&self) -> StemResult<String> {
        if let Some(ref fp) = self.fp {
            Ok(fp.to_string())
        } else {
            Err(StemError::ServerSpecMissingFp)
        }
    }
}

impl CircuitPath for Vec<ServerSpec> {
    fn n_servers(&self) -> usize { self.len() }

    fn join_fps(&self) -> StemResult<String> {
        let mut fps: Vec<&str> = vec![];

        for server_spec in self {
            match server_spec.fp {
                Some(ref fp) => fps.push(fp),
                None => return Err(StemError::ServerSpecMissingFp)
            };
        }

        let joined = fps.join(",");
        Ok(joined)
    }
}

impl<'a> CircuitPath for &'a [ServerSpec] {
    fn n_servers(&self) -> usize { self.len() }

    fn join_fps(&self) -> StemResult<String> {
        let mut fps: Vec<&str> = vec![];

        for server_spec in self.iter() {
            match server_spec.fp {
                Some(ref fp) => fps.push(fp),
                None => return Err(StemError::ServerSpecMissingFp)
            };
        }

        let joined = fps.join(",");
        Ok(joined)
    }
}

impl<'a> CircuitPath for &'a Vec<ServerSpec> {
    fn n_servers(&self) -> usize { self.len() }

    fn join_fps(&self) -> StemResult<String> {
        let mut fps: Vec<&str> = vec![];

        for server_spec in self.iter() {
            match server_spec.fp {
                Some(ref fp) => fps.push(fp),
                None => return Err(StemError::ServerSpecMissingFp)
            };
        }

        let joined = fps.join(",");
        Ok(joined)
    }
}

