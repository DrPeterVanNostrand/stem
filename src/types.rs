use std::fmt::{self, Display, Formatter};
use std::io::Read;
use std::str::FromStr;

use tor_file_parser::{DescIter, Descriptor};

use error::{StemError, StemResult};
use reply::ServerResponseLine;

/// Known types of asynchronous events that the Controller can listen for.
/// For more info regarding Tor events, see
/// [Section 4.1 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
#[derive(Clone, Debug, PartialEq)]
pub enum EventType {
    Circ,
    Stream
}

impl FromStr for EventType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim_left_matches("650 ").split(' ').nth(0).ok_or(())?;

        let event_type = match s {
            "CIRC" => EventType::Circ,
            "STREAM" => EventType::Stream,
            _ => return Err(())
        };

        Ok(event_type)
    }
}

impl EventType {
    pub fn as_str(&self) -> &str {
        match *self {
            EventType::Circ => "CIRC",
            EventType::Stream => "STREAM",
        }
    }
}

impl Display for EventType {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

/// How the Controller identifies a particular Onion Router. Here
/// "ServerSpec" stands for "Server Specifier" and "fp" stands for
/// "fingerprint". Our fingerprints always start with a "$" character.
/// For more information regarding Server Specifiers see
/// [Section 2.4 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
#[derive(Clone, Debug, Default, PartialEq)]
pub struct ServerSpec {
    pub fp: Option<String>,
    pub nick: Option<String>
}

impl ServerSpec {
    pub fn empty() -> Self { ServerSpec::default() }

    /// Encode a `ServerSpec` as a String as defined by
    /// [Section 2.4 of the Tor Control-Spec]
    /// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt)
    /// This method returns an Err when neither a fingerprint nor a
    /// nickname has been set for the instance of `ServerSpec`.
    pub fn long_name(&self) -> StemResult<String> {
        let long_name = {
            if let Some(ref fp) = self.fp {
                if let Some(ref nick) = self.nick {
                    format!("${}~{}", fp, nick)
                } else {
                    format!("${}", fp)
                }
            } else if let Some(ref nick) = self.nick {
                format!("{}", nick)
            } else {
                return Err(StemError::EmptyServerSpecError);
            }
        };

        Ok(long_name)
    }

    /// Instantiate a `ServerSpec` from a fingerprint.
    pub fn from_fp(fp: &str) -> Self {
        let fp = if fp.starts_with('$') {
            Some(fp.to_string())
        } else {
            Some(format!("${}", fp))
        };

        ServerSpec { fp, nick: None }
    }

    /// Instantiate a `ServerSpec` from a nickname.
    pub fn from_nick(nick: &str) -> Self {
        ServerSpec { fp: None, nick: Some(nick.to_string()) }
    }
}

/// Decodes a string into an instance of `ServerSpec` as defined by
/// [Section 2.4 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
impl FromStr for ServerSpec {
    type Err = StemError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.is_empty() {
            return Err(StemError::ServerSpecFromStrError);
        }

        let mut server_spec = ServerSpec::empty();

        let split: Vec<_> = s.splitn(2, ' ').nth(0).unwrap()
            .split(|c| c == '~' || c == '=')
            .collect();

        let n_tokens = split.len();
        let has_fp = s.starts_with('$');

        if n_tokens == 1 {
            let first = Some(split[0].to_string());

            if has_fp {
                server_spec.fp = first;
            } else {
                server_spec.nick = first;
            }
        } else if n_tokens == 2 && has_fp {
            server_spec.fp = Some(split[0].to_string());
            server_spec.nick = Some(split[1].to_string());
        } else {
            return Err(StemError::ServerSpecFromStrError);
        }

        Ok(server_spec)
    }
}

/// Converts a parsed Descriptor into a ServerSpec.
impl From<Descriptor> for ServerSpec {
    fn from(desc: Descriptor) -> Self {
        let fp = desc.get_fingerprint().map(|fp|
            if !fp.starts_with('$') {
                format!("${}", fp)
            } else {
                fp
            }
        );
        let nick = desc.r.map(|r| r.nick);
        ServerSpec { fp, nick }
    }
}

#[derive(Debug)]
pub struct ServerSpecIter<R: Read>(DescIter<R>);

impl<R: Read> ServerSpecIter<R> {
    pub fn new(desc_iter: DescIter<R>) -> Self {
        ServerSpecIter(desc_iter)
    }
}

impl<R: Read> Iterator for ServerSpecIter<R> {
    type Item = ServerSpec;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|desc| ServerSpec::from(desc))
    }
}

// An interface for treating both a single `ServerSpec` and an array of
// `ServerSpec`s as a Circuit Path. Circuit paths contain one or more Onion
// Routers.
trait CircuitPath {
    fn n_servers(&self) -> usize;
    fn is_empty(&self) -> bool { self.n_servers() == 0 }
    fn join_fps(&self) -> StemResult<String>;
}

impl CircuitPath for ServerSpec {
    fn n_servers(&self) -> usize { 1 }

    fn join_fps(&self) -> StemResult<String> {
        if let Some(ref fp) = self.fp {
            Ok(fp.to_string())
        } else {
            Err(StemError::ServerSpecMissingFp)
        }
    }
}

impl<'a> CircuitPath for &'a ServerSpec {
    fn n_servers(&self) -> usize { 1 }

    fn join_fps(&self) -> StemResult<String> {
        if let Some(ref fp) = self.fp {
            Ok(fp.to_string())
        } else {
            Err(StemError::ServerSpecMissingFp)
        }
    }
}

impl CircuitPath for Vec<ServerSpec> {
    fn n_servers(&self) -> usize { self.len() }

    fn join_fps(&self) -> StemResult<String> {
        let mut fps: Vec<&str> = vec![];

        for server_spec in self {
            match server_spec.fp {
                Some(ref fp) => fps.push(fp),
                None => return Err(StemError::ServerSpecMissingFp)
            };
        }

        let joined = fps.join(",");
        Ok(joined)
    }
}

impl<'a> CircuitPath for &'a [ServerSpec] {
    fn n_servers(&self) -> usize { self.len() }

    fn join_fps(&self) -> StemResult<String> {
        let mut fps: Vec<&str> = vec![];

        for server_spec in self.iter() {
            match server_spec.fp {
                Some(ref fp) => fps.push(fp),
                None => return Err(StemError::ServerSpecMissingFp)
            };
        }

        let joined = fps.join(",");
        Ok(joined)
    }
}

/// The possible values for a Circuit Event's "CircStatus" field. For more
/// information regarding Circuit Events and Circuit Statuses see
/// [Section 4.1.1 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
#[derive(Debug, PartialEq)]
pub enum CircuitStatus {
    Built,
    Closed,
    Extended,
    Failed,
    GuardWait,
    Launched
}

impl FromStr for CircuitStatus {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let status = match s {
            "BUILT" => CircuitStatus::Built,
            "CLOSED" => CircuitStatus::Closed,
            "EXTENDED" => CircuitStatus::Extended,
            "FAILED" => CircuitStatus::Failed,
            "GUARD_WAIT" => CircuitStatus::GuardWait,
            "LAUNCHED" => CircuitStatus::Launched,
            _ => return Err(())
        };

        Ok(status)
    }
}

/// The possible values that may be contained by a Circuit Event's
/// "Build Flags" list. For more information regarding Circuit Events and
/// Circuit Build Flags see
/// [Section 4.1.1 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
#[derive(Debug, PartialEq)]
pub enum CircuitBuildFlag {
    OneHopTunnel,
    IsInternal,
    NeedCapacity,
    NeedUptime
}

impl FromStr for CircuitBuildFlag {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let flag = match s {
            "ONEHOP_TUNNEL" => CircuitBuildFlag::OneHopTunnel,
            "IS_INTERNAL" => CircuitBuildFlag::IsInternal,
            "NEED_CAPACITY" => CircuitBuildFlag::NeedCapacity,
            "NEED_UPTIME" => CircuitBuildFlag::NeedUptime,
            _ => return Err(())
        };

        Ok(flag)
    }
}

/// The possible values for a Circuit Event's "Purpose" field. For more
/// information regarding Circuit Events and Circuit Purposes see
/// [Section 4.1.1 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
#[derive(Debug)]
pub enum CircuitPurpose {
    General,
    HsClientIntro,
    HsClientRend,
    HsServiceIntro,
    HsServiceRend,
    Testing,
    Controller,
    MeasureTimeout
}

impl FromStr for CircuitPurpose {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim_left_matches("PURPOSE=");

        let purpose = match s {
            "GENERAL" => CircuitPurpose::General,
            "HS_CLIENT_INTRO" => CircuitPurpose::HsClientIntro,
            "HS_CLIENT_REND" => CircuitPurpose::HsClientRend,
            "HS_SERVICE_INTRO" => CircuitPurpose::HsServiceIntro,
            "HS_SERVICE_REND" => CircuitPurpose::HsServiceRend,
            "TESTING" => CircuitPurpose::Testing,
            "CONTROLLER" => CircuitPurpose::Controller,
            "MEASURE_TIMEOUT" => CircuitPurpose::MeasureTimeout,
            _ => return Err(())
        };

        Ok(purpose)
    }
}

impl CircuitPurpose {
    pub fn as_str(&self) -> &str {
        match *self {
            CircuitPurpose::General => "GENERAL",
            CircuitPurpose::HsClientIntro => "HS_CLIENT_INTRO",
            CircuitPurpose::HsClientRend => "HS_CLIENT_REND",
            CircuitPurpose::HsServiceIntro => "HS_SERVICE_INTRO",
            CircuitPurpose::HsServiceRend => "HS_SERVICE_REND",
            CircuitPurpose::Testing => "TESTING",
            CircuitPurpose::Controller => "CONTROLLER",
            CircuitPurpose::MeasureTimeout => "MEASURE_TIMEOUT",
        }
    }
}

impl Display for CircuitPurpose {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

#[derive(Debug)]
pub enum HiddenServiceState {
    HsciConnecting,
    HsciIntroSent,
    HsciDone,
    HscrConnecting,
    HscrEstablishedIdle,
    HscrEstablishedWaiting,
    HscrJoined,
    HssiConnecting,
    HssiEstablished,
    HssrConnecting,
    HssrJoined
}

impl FromStr for HiddenServiceState {
    type Err = StemError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim_left_matches("HS_STATE=");

        let hs_state = match s {
            "HSCI_CONNECTING" => HiddenServiceState::HsciConnecting,
            "HSCI_INTRO_SENT" => HiddenServiceState::HsciIntroSent,
            "HSCI_DONE" => HiddenServiceState::HsciDone,
            "HSCR_CONNECTING" => HiddenServiceState::HscrConnecting,
            "HSCR_ESTABLISHED_IDLE" => HiddenServiceState::HscrEstablishedIdle,
            "HSCR_ESTABLISHED_WAITING" => HiddenServiceState::HscrEstablishedWaiting,
            "HSCR_JOINED" => HiddenServiceState::HscrJoined,
            "HSSI_CONNECTING" => HiddenServiceState::HssiConnecting,
            "HSSI_ESTABLISHED" => HiddenServiceState::HssiEstablished,
            "HSSR_CONNECTING" => HiddenServiceState::HssrConnecting,
            "HSSR_JOINED" => HiddenServiceState::HssrJoined,
            _ => return Err(StemError::InvalidHiddenServiceState)
        };

        Ok(hs_state)
    }
}

impl HiddenServiceState {
    pub fn as_str(&self) -> &str {
        match *self {
            HiddenServiceState::HsciConnecting => "HSCI_CONNECTING",
            HiddenServiceState::HsciIntroSent => "HSCI_INTRO_SENT",
            HiddenServiceState::HsciDone => "HSCI_DONE",
            HiddenServiceState::HscrConnecting => "HSCR_CONNECTING",
            HiddenServiceState::HscrEstablishedIdle => "HSCR_ESTABLISHED_IDLE",
            HiddenServiceState::HscrEstablishedWaiting => "HSCR_ESTABLISHED_WAITING",
            HiddenServiceState::HscrJoined => "HSCR_JOINED",
            HiddenServiceState::HssiConnecting => "HSSI_CONNECTING",
            HiddenServiceState::HssiEstablished => "HSSI_ESTABLISHED",
            HiddenServiceState::HssrConnecting => "HSSR_CONNECTING",
            HiddenServiceState::HssrJoined => "HSSR_JOINED"
        }
    }
}

impl Display for HiddenServiceState {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

/// The possible values for a Circuit Event's "Reason" and "Remote Reason"
/// fields. For more information regarding Circuit Events and Circuit
/// Reasons see
/// [Section 4.1.1 of the Tor Control-Spec]
/// (https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).
#[derive(Debug)]
pub enum CircuitReason {
    None,
    TorProtocol,
    Internal,
    Requested,
    Hibernating,
    ResourceLimit,
    ConnectFailed,
    OrIdentity,
    OrConnClosed,
    Timeout,
    Finished,
    Destroyed,
    NoPath,
    NoSuchService,
    MeasurementExpired
}

impl FromStr for CircuitReason {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim_left_matches("REMOTE_").trim_left_matches("REASON");

        let reason = match s {
            "NONE" => CircuitReason::None,
            "TORPROTOCOL" => CircuitReason::TorProtocol,
            "INTERNAL" => CircuitReason::Internal,
            "REQUESTED" => CircuitReason::Requested,
            "HIBERNATING" => CircuitReason::Hibernating,
            "RESOURCELIMIT" => CircuitReason::ResourceLimit,
            "CONNECTFAILED" => CircuitReason::ConnectFailed,
            "OR_IDENTITY" => CircuitReason::OrIdentity,
            "OR_CONN_CLOSED" => CircuitReason::OrConnClosed,
            "TIMEOUT" => CircuitReason::Timeout,
            "FINISHED" => CircuitReason::Finished,
            "DESTROYED" => CircuitReason::Destroyed,
            "NOPATH" => CircuitReason::NoPath,
            "NOSUCHSERVICE" => CircuitReason::NoSuchService,
            "MEASUREMENT_EXPIRED" => CircuitReason::MeasurementExpired,
            _ => return Err(())
        };

        Ok(reason)
    }
}

#[derive(Debug)]
pub struct CircuitEvent {
    pub id: u16,
    pub status: CircuitStatus,
    pub path: Option<Vec<ServerSpec>>,
    pub build_flags: Option<Vec<CircuitBuildFlag>>,
    pub purpose: Option<CircuitPurpose>,
    pub hs_state: Option<HiddenServiceState>,
    pub rend_query: Option<String>,
    pub time_created: Option<String>,
    pub reason: Option<CircuitReason>,
    pub remote_reason: Option<CircuitReason>
}

impl CircuitEvent {
    pub fn from_str<T>(s: &T) -> Self
        where T: ServerResponseLine + AsRef<str>
    {
        let s = s.as_ref().trim_left_matches("650 CIRC ");
        let (args, kwargs) = s.split_into_args_and_kwargs();

        let id = args[0].parse().unwrap();
        let status = CircuitStatus::from_str(&args[1]).unwrap();

        let path = args.iter()
            .position(|arg| arg.starts_with('$'))
            .map(|i| {
                let mut path = vec![];
                for token in args[i].split(',') {
                    let server = ServerSpec::from_str(&token).unwrap();
                    path.push(server);
                }
                path
            });

        let build_flags = kwargs.get("BUILD_FLAGS")
            .map(|value| {
                let mut build_flags = vec![];
                for token in value.split(',') {
                    let flag = CircuitBuildFlag::from_str(&token).unwrap();
                    build_flags.push(flag);
                }
                build_flags
            });

        let purpose = kwargs.get("PURPOSE")
            .map(|value| CircuitPurpose::from_str(value).unwrap());

        let hs_state = kwargs.get("HS_STATE")
            .map(|value| HiddenServiceState::from_str(value).unwrap());

        let rend_query = kwargs.get("REND_QUERY")
            .map(|value| value.to_string());

        let time_created = kwargs.get("TIME_CREATED")
            .map(|value| value.to_string());

        let reason = kwargs.get("REASON")
            .map(|value| CircuitReason::from_str(value).unwrap());

        let remote_reason = kwargs.get("REMOTE_REASON")
            .map(|value| CircuitReason::from_str(value).unwrap());

        CircuitEvent {
            id, status, path,
            build_flags, purpose, hs_state,
            rend_query, time_created, reason,
            remote_reason
        }
    }
}

impl CircuitEvent {
    pub fn is_built(&self) -> bool {
        self.status == CircuitStatus::Built
    }

    pub fn has_failed(&self) -> bool {
        self.status == CircuitStatus::Failed
    }

    pub fn is_internal(&self) -> bool {
        if let Some(ref flags) = self.build_flags {
            flags.contains(&CircuitBuildFlag::IsInternal)
        } else {
            false
        }
    }

    pub fn is_one_hop_tunnel(&self) -> bool {
        if let Some(ref flags) = self.build_flags {
            flags.contains(&CircuitBuildFlag::OneHopTunnel)
        } else {
            false
        }
    }

    pub fn get_guard(&self) -> Option<&ServerSpec> {
        if let Some(ref path) = self.path {
            path.first()
        } else {
            None
        }
    }

    /// Get the last server in the Circuit's path. If the circuit has the
    /// "ONEHOP_TUNNEL" build flag, we consider the one server in the
    /// Circuit's path to be a guard not an exit.
    pub fn get_exit(&self) -> Option<&ServerSpec> {
        if !self.is_one_hop_tunnel() {
            if let Some(ref path) = self.path {
                return path.last();
            }
        }

        None
    }

    pub fn n_servers(&self) -> usize {
        match self.path {
            Some(ref path) => path.n_servers(),
            None => 0
        }
    }
}

impl Display for CircuitEvent {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:#?}", self)
    }
}
