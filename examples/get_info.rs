extern crate stem;

use stem::TorCtrl;

fn main() {
    let mut tc = TorCtrl::new(9051).unwrap();
    let _ = tc.auth(None).unwrap();
    println!("\nRunning `GETINFO circuit-status`...");
    let reply = tc.get_info("circuit-status").unwrap();
    println!("Reply status-code => {}\n", reply.status_code);
    for line in reply.lines() {
        println!("{}", line);        
    }
}
