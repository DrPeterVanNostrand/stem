extern crate stem;

use stem::{TorCtrl, TorProcess};

fn main() {
    let _tor = TorProcess::launch().unwrap();
    let mut tc = TorCtrl::new(9051).unwrap();
    println!("AUTHENTICATE => {:?}", tc.auth(None));
}
