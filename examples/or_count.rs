extern crate stem;

use stem::TorCtrl;

fn main() {
    let mut tc = TorCtrl::new(9051).unwrap();
    let _ = tc.auth(None).unwrap();
    println!("N EXITS {}", tc.all_exits().unwrap().len());
    println!("N GUARDS {}", tc.all_guards().unwrap().len());
    println!("N ROUTERS {}", tc.all_routers().unwrap().count());
}
