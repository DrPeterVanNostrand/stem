extern crate stem;

use stem::{CircuitPath, TorCtrl};

fn main() {
    let mut tc = TorCtrl::new(9051).unwrap();
    let _ = tc.auth(None).unwrap();
    println!("Using Relays:");

    for circuit in tc.get_circuits().unwrap() {
        if let Some(ref path) = circuit.path {
            let n_servers = path.n_servers();
            if n_servers < 3 { continue; }
            for relay in &path[1..n_servers - 1] {
                println!("{:?}", relay);
            }
        }
    }
}