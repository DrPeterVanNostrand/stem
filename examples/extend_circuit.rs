extern crate stem;

use stem::TorCtrl;

fn main() {
    let mut tc = TorCtrl::new(9051).unwrap();
    let _ = tc.auth(None).unwrap();

    // The circuit that we will extend.
    let circuit = tc.get_non_internal_circuits().unwrap()
        .into_iter()
        .nth(0).unwrap();
        
    // The number of OR's in the circuit's path. 
    let initial_path_length = circuit.n_servers();

    // The OR that we will extend the circuit with.
    let new_exit = tc.get_exits_in_use().unwrap()
        .into_iter()
        .filter(|exit| exit != circuit.get_exit().unwrap())
        .nth(0).unwrap(); 

    println!("Extending Circuit #{}...", circuit.id);
    println!("Initial path length => {}", initial_path_length);
    println!("Attempting to extend circuit with OR => {:?}", new_exit);
    println!("Running EXTENDCIRCUIT...");

    let result = tc.extend_circuit(circuit.id, new_exit);

    if let Err(error) = result {
        println!("An error occurred: {:?}", error);
    } else {
        // Get the updated circuit data.
        let circuit = tc.get_circuit_by_id(circuit.id).unwrap();
        println!("Successfully extended circuit #{}", circuit.id);

        // Check to see if the circuit's path length has been incremented.
        let new_path_length = circuit.n_servers();
        println!("Final path length => {}", new_path_length);        
    }
}
