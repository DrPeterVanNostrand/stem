# stem

A Tor controller implemented in Rust.

# About

This crate allows you to control various Tor features such as circuit
creation and extension, Tor configuration manipulation, and Tor event
monitoring.

This crate communicates with a running Tor process via the
[Tor Control Protocol](https://gitweb.torproject.org/torspec.git/tree/control-spec.txt).

Much of this project has been a feature clone of the Tor Project's
[Stem](https://gitweb.torproject.org/stem.git/tree/stem), a popular and
well maintained Tor controller written in Python.

# Installation

To use this crate from a project of your own, add the following to your
project's `Cargo.toml`:

    [dependencies]
    stem = { git = "https://gitlab.com/DrPeterVanNostrand/stem" }

# Usage

```rust
extern crate stem;

use stem::{TorCtrl, TorProcess};

fn main() {
    let _tor = TorProcess::launch().unwrap();
    let mut tc = TorCtrl::new(9051).unwrap();
    let _ = tc.auth(None).unwrap();

    if let Ok(reply) = tc.get_info("circuit-status") {
        for line in reply.lines() {
            println!("{}", line);
        }
    }
}
```

Other examples can be found in the
[`examples` directory](https://gitlab.com/DrPeterVanNostrand/stem/tree/master/examples).

# Contributing

Details on how to contribute can be found in `CONTRIBUTING.md`.

# TODO

This crate is not a complete Tor controller, notably it's missing
functionality for:

1. Cookie and SafeCookie authentication
2. Controlling hidden services
3. Controlling streams
4. Allow the user to supply their own handlers for Tor Asynchronous Events
